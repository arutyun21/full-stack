import React, { Component } from 'react'
import {connect} from "react-redux";
import classnames from "classnames";
import {Link} from "react-router-dom";
import PropTypes from "prop-types";
import {getProjectTask} from "../../actions/projectTaskActions"

class UpdateProjectTask extends Component {
    
    componentDidMount(){
        const {pt_id} = this.props.match.params
        this.props.getProjectTask(pt_id);
    }
    
    render() {
        return (
            <div className="addProjectTask">
                <div className="container">
                    <div className="row">
                        <div className="col-md-8 m-auto">
                            <a href="/" className="btn btn-light">
                                Back to Board
                            </a>
                            <h4 className="display-4 text-center">Add /Update Project Task</h4>
                            <form>
                                <div className="form-group">
                                    <input type="text" className="form-control form-control-lg" name="summary" placeholder="Project Task summary" />
                                </div>
                                <div className="form-group">
                                    <textarea className="form-control form-control-lg" placeholder="Acceptance Criteria" name="acceptanceCriteria"></textarea>
                                </div>
                                <div className="form-group">
                                    <select className="form-control form-control-lg" name="status">
                                        <option value="">Select Status</option>
                                        <option value="TO_DO">TO DO</option>
                                        <option value="IN_PROGRESS">IN PROGRESS</option>
                                        <option value="DONE">DONE</option>
                                    </select>
                                </div>
                                <input type="submit" className="btn btn-primary btn-block mt-4" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}


UpdateProjectTask.PropTypes = {
    project_task: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired,
    getProjectTask: PropTypes.func.isRequired
}

const mapStateToProps = state =>({
    project_task: state.project_task,
    errors: state.errors
    
});

export default connect(mapStateToProps,{getProjectTask}) (UpdateProjectTask);